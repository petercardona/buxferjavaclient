package com.rathravane.buxferClient;

import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.TestCase;

@Ignore	// we can't run repeated logins against buxfer
public class SumAndAvgOverDaysTest extends TestCase
{
	@Test
	public void testSum () throws IOException
	{
		final BuxferClient client = new BuxferClientFactory ()
			.withLoginUserId ( System.getenv ( "BUXFER_USERID" ) )
			.withLoginPassword ( System.getenv ( "BUXFER_PASSWORD" ) )
			.build ()
		;

		final int dayCount = 30;
		final Summer summer = new Summer ();

		client.getTransactions ( 
			new BuxferTransactionFilter ()
				.onAccountWithId ( System.getenv ( "BUXFER_ACCTID" ) )
				.betweenYesterdayAndPriorDays ( dayCount )
			,
			summer
		);

		final double avg = summer.getTotal () / dayCount;

		log.info ( "Sum for last " + dayCount + " days ending yesterday: "  + summer.getTotal () );
		log.info ( "Avg for last " + dayCount + " days ending yesterday: "  + avg );
	}

	private static final Logger log = LoggerFactory.getLogger ( SumAndAvgOverDaysTest.class );

	private static class Summer implements BuxferCallback<BuxferTransaction>
	{
		private double fTotal = 0.0;

		@Override
		public void handle ( BuxferTransaction t )
		{
			fTotal += t.getAmount ().toDouble ();
		}

		public double getTotal () { return fTotal; }
		
	}
}
