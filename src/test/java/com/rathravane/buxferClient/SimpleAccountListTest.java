package com.rathravane.buxferClient;

import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.TestCase;

@Ignore	// we can't run repeated logins against buxfer
public class SimpleAccountListTest extends TestCase
{
	@Test
	public void testAccountListFetch () throws IOException
	{
		final BuxferClient client = new BuxferClientFactory ()
			.withLoginUserId ( System.getenv ( "BUXFER_USERID" ) )
			.withLoginPassword ( System.getenv ( "BUXFER_PASSWORD" ) )
			.build ()
		;
		for ( BuxferAccount acct : client.getAccounts ( ) )
		{
			log.info ( acct.getId () + " / " + acct.getName () );
		}
	}

	private static final Logger log = LoggerFactory.getLogger ( SimpleAccountListTest.class );
}
