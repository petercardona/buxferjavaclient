package com.rathravane.buxferClient;

public interface BuxferAccount
{
	String getId ();

	String getName ();
	
	String getBank ();
	
	BuxferAmount getBalance ();

	/**
	 * Get the last sync time in epoch seconds.
	 * @return the last sync time, -1 if unavailable
	 */
	long getLastSync ();
}
