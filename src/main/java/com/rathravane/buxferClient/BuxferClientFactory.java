package com.rathravane.buxferClient;

import java.io.IOException;

import com.rathravane.buxferClient.impl.BuxferClientImpl;

public class BuxferClientFactory
{
	/**
	 * Specify the user ID for login. Buxfer allows email address or username here.
	 * @param user
	 * @return this factory
	 */
	public BuxferClientFactory withLoginUserId ( String user )
	{
		fUser = user;
		return this;
	}

	/**
	 * Specify the password for the given user. 
	 * @param password
	 * @return this factory
	 */
	public BuxferClientFactory withLoginPassword ( String password )
	{
		fPassword = password;
		return this;
	}

	/**
	 * Build a client instance.
	 * @return a Buxfer client
	 * @throws IOException 
	 */
	public BuxferClient build () throws IOException
	{
		if ( fUser == null || fPassword == null )
		{
			throw new IllegalArgumentException ( "Please set username and password before building the client." );
		}
		return new BuxferClientImpl ( this );
	}

	private String fUser;
	private String fPassword;

	public String getUserId () { return fUser; }
	public String getPassword () { return fPassword; }
}
