package com.rathravane.buxferClient;

public interface BuxferCallback<T>
{
	public class CancelIterationException extends Exception
	{
		public CancelIterationException ( String msg ) { super ( msg ); }
		public CancelIterationException ( Throwable t ) { super ( t ); }
		public CancelIterationException ( String msg, Throwable t ) { super ( msg, t ); }

		private static final long serialVersionUID = 1L;
	}

	/**
	 * Handle an instance from the stream of instances. 
	 * @param t
	 * @throws CancelIterationException to stop iterating
	 */
	void handle ( T t ) throws CancelIterationException;
}
