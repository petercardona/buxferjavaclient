package com.rathravane.buxferClient.impl;

import com.rathravane.buxferClient.BuxferAmount;

public class BuxferAmountImpl implements BuxferAmount
{
	public BuxferAmountImpl ( double value )
	{
		fCents = Math.round ( value * 100.0 );
	}

	@Override
	public double toDouble ()
	{
		return fCents / 100.0;
	}

	@Override
	public long toCents ()
	{
		return fCents;
	}

	@Override
	public String toString ()
	{
		return "" + toDouble();	// FIXME
	}

	private final long fCents;
}
