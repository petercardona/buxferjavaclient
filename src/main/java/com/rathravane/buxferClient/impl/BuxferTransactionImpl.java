package com.rathravane.buxferClient.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rathravane.buxferClient.BuxferAmount;
import com.rathravane.buxferClient.BuxferClient;
import com.rathravane.buxferClient.BuxferTransaction;

public class BuxferTransactionImpl implements BuxferTransaction
{
	public BuxferTransactionImpl ( JSONObject data )
	{
		fData = data;
	}

	@Override
	public String getId ()
	{
		return Long.toString ( fData.getLong ( "id" ) );
	}

	@Override
	public String getAcctId ()
	{
		return fData.getString ( "accountId" );
	}

	@Override
	public String getDescription ()
	{
		return fData.getString ( "description" );
	}

	@Override
	public long getDate ()
	{
		final String date = fData.optString ( "normalizedDate", null );
		if ( date != null )
		{
			try
			{
				final SimpleDateFormat sdf = new SimpleDateFormat ( "yyyy-MM-dd" );
				sdf.setTimeZone ( TimeZone.getTimeZone ( "US/Pacific" ) );
				return sdf.parse ( date ).getTime () / 1000L;
			}
			catch ( ParseException e )
			{
				log.warn ( "Couldn't parse [" + date + "] for epoch time." );
			}
		}
		return -1L;
	}

	@Override
	public BuxferAmount getAmount ()
	{
		return new BuxferAmountImpl ( fData.getDouble ( "expenseAmount" ) );
	}

	@Override
	public List<String> getTags ()
	{
		final LinkedList<String> result = new LinkedList<> ();
		final JSONArray array = fData.optJSONArray ( "tagNames" );
		if ( array == null ) return result;

		for ( int i=0; i<array.length(); i ++ )
		{
			result.add ( array.get ( i ).toString () );
		}
		return result;
	}

	private final JSONObject fData;
	private static final Logger log = LoggerFactory.getLogger ( BuxferClient.class );
}
