package com.rathravane.buxferClient.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.rathravane.buxferClient.BuxferAccount;
import com.rathravane.buxferClient.BuxferCallback;
import com.rathravane.buxferClient.BuxferCallback.CancelIterationException;
import com.rathravane.buxferClient.BuxferClient;
import com.rathravane.buxferClient.BuxferClientFactory;
import com.rathravane.buxferClient.BuxferTransaction;
import com.rathravane.buxferClient.BuxferTransactionFilter;

public class BuxferClientImpl implements BuxferClient
{
	public BuxferClientImpl ( BuxferClientFactory factory ) throws IOException
	{
		fToken = login ( factory.getUserId(), factory.getPassword () );
	}

	@Override
	public void logout ()
	{
		// this is a no-op
	}
	
	@Override
	public Collection<BuxferAccount> getAccounts () throws IOException
	{
		final LinkedList<BuxferAccount> result = new LinkedList<> ();
		getAccounts ( new BuxferCallback<BuxferAccount> ()
		{
			@Override
			public void handle ( BuxferAccount t )
			{
				result.add ( t );
			}
		} );
		return result;
	}

	public void getAccounts ( BuxferCallback<BuxferAccount> handler ) throws IOException
	{
		// the accounts request takes no arguments and the response is not paginated
		final HttpResponse<JsonNode> response = runAuthdApiCall ( "accounts" );
		if ( response.getStatus () < 200 || response.getStatus () >= 300 )
		{
			throw new IOException ( "Call failed: " + response.getStatusText () );
		}

		final JSONArray acctList = response.getBody ().getObject ().getJSONObject("response").optJSONArray ( "accounts" );
		try
		{
			for ( int i=0; i<acctList.length (); i++ )
			{
				final JSONObject acctObject = acctList.getJSONObject ( i );
				final BuxferAccount acct = new BuxferAccountImpl ( acctObject );
				handler.handle ( acct );
			}
		}
		catch ( JSONException e )
		{
			log.warn ( "JSON parse problem: " + e.getMessage () );
		}
		catch ( CancelIterationException e )
		{
			log.info ( "Client cancelled iteration: " + e.getMessage () );
		}
	}

	@Override
	public void getTransactions ( BuxferTransactionFilter filter, final BuxferCallback<BuxferTransaction> handler ) throws IOException
	{
		// the accounts request takes no arguments and the response is not paginated
		final Map<String,String> args = filter == null ? new HashMap<String,String>() : filter.toQueryArgs ();
		runAuthdApiCallToStream ( "transactions", args,
			new PagingIterator<BuxferTransaction> (
				new FactoryAndCallback<BuxferTransaction> ()
				{
					@Override
					public BuxferTransaction create ( JSONObject data )
					{
						return new BuxferTransactionImpl ( data );
					}

					@Override
					public BuxferCallback<BuxferTransaction> getCallback ()
					{
						return handler;
					}
					
				},
				"transactions" ) );
	}

	private final String fToken;

	private String login ( String userId, String password ) throws IOException
	{
		final HashMap<String,String> args = new HashMap<> ();
		args.put ( "userid", userId );
		args.put ( "password", password );
		final HttpResponse<JsonNode> result = runApiCall ( "login", args );
		if ( result.getStatus () >= 200 && result.getStatus () <= 299 )
		{
			return result.getBody ().getObject ().getJSONObject("response").getString ( "token" );
		}
		else
		{
			throw new IOException ( "Login failed. " + result.getStatusText() );
		}
	}

	private HttpResponse<JsonNode> runAuthdApiCall ( String path ) throws IOException
	{
		return runAuthdApiCall ( path, new HashMap<String,String> () );
	}

	private HttpResponse<JsonNode> runAuthdApiCall ( String path, Map<String, String> args ) throws IOException
	{
		args.put ( "token", fToken );
		return runApiCall ( path, args );
	}

	private static HttpResponse<JsonNode> runApiCall ( String path, Map<String, String> args ) throws IOException
	{
		try
		{
			final StringBuilder sb = new StringBuilder ()
				.append ( kBaseUrl )
				.append ( path )
			;

			return Unirest.get ( sb.toString () )
				.header ( "Content-type", "application/json" )
				.header ( "accept", "application/json" )
				.queryString ( convertMap ( args ) )
				.asJson ()
			;
		}
		catch ( UnirestException x )
		{
			throw new IOException ( x );
		}
	}

	private interface FactoryAndCallback<T>
	{
		T create ( JSONObject data );
		BuxferCallback<T> getCallback ();
	}
	
	private class PagingIterator<T>
	{
		public PagingIterator ( FactoryAndCallback<T> handler, String responseElement )
		{
			fHandler = handler;
			fResponseElement = responseElement;
		}

		/**
		 * @param response
		 * @return true if complete
		 */
		public boolean handle ( HttpResponse<JsonNode> response ) throws CancelIterationException
		{
			if ( response.getStatus () < 200 || response.getStatus () >= 300 )
			{
				throw new CancelIterationException ( "Call failed: " + response.getStatusText() );
			}

			final JSONArray trxList = response.getBody ().getObject ().getJSONObject("response").optJSONArray ( fResponseElement );
			if ( trxList.length () == 0 )
			{
				log.info ( "Reached end of list." );
				return true;
			}

			try
			{
				for ( int i=0; i<trxList.length (); i++ )
				{
					final JSONObject trxObj = trxList.getJSONObject ( i );
					final T trx = fHandler.create ( trxObj );
					fHandler.getCallback ().handle ( trx );
				}
			}
			catch ( JSONException e )
			{
				log.warn ( "JSON parse problem: " + e.getMessage () );
			}

			return false;
		}

		private final FactoryAndCallback<T> fHandler;
		private final String fResponseElement;
	}
	
	private void runAuthdApiCallToStream ( String path, Map<String, String> args, PagingIterator<?> callback ) throws IOException
	{
		boolean done = false;
		int page = 0;

		while ( !done )
		{
			page++;
			args.put ( "page", "" + page );

			final HttpResponse<JsonNode> result = runAuthdApiCall ( path, args );
			try
			{
				done = callback.handle ( result );
			}
			catch ( CancelIterationException e )
			{
				log.info ( "Iteration cancelled. " + e.getMessage () );
				done = true;
			}
		}

	}

	private static final String kBaseUrl = "https://www.buxfer.com/api/";
	private static final Logger log = LoggerFactory.getLogger ( BuxferClient.class );

	private static Map<String,Object> convertMap ( Map<String,String> map )
	{
		if ( map == null ) return null;
		final HashMap<String,Object> result = new HashMap<> ();
		for ( Map.Entry<String, String> e : map.entrySet () )
		{
			result.put ( e.getKey (), e.getValue () );
		}
		return result;
	}
}
