package com.rathravane.buxferClient.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rathravane.buxferClient.BuxferAccount;
import com.rathravane.buxferClient.BuxferAmount;
import com.rathravane.buxferClient.BuxferClient;

public class BuxferAccountImpl implements BuxferAccount
{
	public BuxferAccountImpl ( JSONObject data )
	{
		fData = data;
	}

	@Override
	public String getId ()
	{
		return Long.toString ( fData.getLong ( "id" ) );
	}

	@Override
	public String getName ()
	{
		return fData.getString ( "name" );
	}

	@Override
	public String getBank ()
	{
		return fData.getString ( "bank" );
	}

	@Override
	public BuxferAmount getBalance ()
	{
		return new BuxferAmountImpl ( fData.getDouble ( "balance" ) );
	}

	@Override
	public long getLastSync ()
	{
		final String sync = fData.optString ( "lastSynced", null );
		if ( sync != null )
		{
			try
			{
				final SimpleDateFormat sdf = new SimpleDateFormat ( "yyyy-MM-dd hh:mm:ss" );
				sdf.setTimeZone ( TimeZone.getTimeZone ( "US/Pacific" ) );
				return sdf.parse ( sync ).getTime () / 1000L;
			}
			catch ( ParseException e )
			{
				log.warn ( "Couldn't parse [" + sync + "] for timestamp." );
			}
		}
		return -1L;
	}

	private final JSONObject fData;
	private static final Logger log = LoggerFactory.getLogger ( BuxferClient.class );
}
