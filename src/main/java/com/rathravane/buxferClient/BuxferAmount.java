package com.rathravane.buxferClient;

public interface BuxferAmount
{
	double toDouble ();

	long toCents ();

	String toString ();
}
