package com.rathravane.buxferClient;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BuxferTransactionFilter
{
	public BuxferTransactionFilter () {}

	public BuxferTransactionFilter onAccountWithId ( String id )
	{
		fAcctId = id;
		fAcctName = null;
		return this;
	}

	public BuxferTransactionFilter onAccountNamed ( String name )
	{
		fAcctId = null;
		fAcctName = name;
		return this;
	}

	public BuxferTransactionFilter withTagId ( String tagId )
	{
		fTagId = tagId;
		fTagName = null;
		return this;
	}

	public BuxferTransactionFilter withTagName ( String tagName )
	{
		fTagId = null;
		fTagName = tagName;
		return this;
	}

	public BuxferTransactionFilter onDate ( String date )
	{
		return between ( date, date );
	}

	public BuxferTransactionFilter onDate ( long dateEpochMs )
	{
		onDate ( epochMsToString ( dateEpochMs ) );
		return this;
	}

	public BuxferTransactionFilter onDate ( Date date )
	{
		onDate ( dateToString ( date ) );
		return this;
	}

	public BuxferTransactionFilter between ( String startDate, String endDate )
	{
		fStartDate = startDate;
		fEndDate = endDate;
		return this;
	}

	public BuxferTransactionFilter between ( long startDateEpochMs, long endDateEpochMs )
	{
		return between ( epochMsToString ( startDateEpochMs ), epochMsToString ( endDateEpochMs ) );
	}

	public BuxferTransactionFilter between ( Date startDate, Date endDate )
	{
		return between ( dateToString ( startDate ), dateToString ( endDate ) );
	}

	public BuxferTransactionFilter inLastDays ( int dayCount )
	{
		if ( dayCount < 0 ) throw new IllegalArgumentException ( "Day count must be positive." );

		final long nowMs = System.currentTimeMillis ();
		final long dayOfMs = 1000 * 60 * 60 * 24;
		final long durationMs = dayOfMs * dayCount;
		final long startMs = nowMs - durationMs;
		
		return between ( startMs, nowMs );
	}

	public BuxferTransactionFilter betweenYesterdayAndPriorDays ( int dayCount )
	{
		if ( dayCount < 0 ) throw new IllegalArgumentException ( "Day count must be positive." );

		final long nowMs = System.currentTimeMillis ();
		final long dayOfMs = 1000 * 60 * 60 * 24;
		final long yesterdayMs = nowMs - dayOfMs;
		final long durationMs = dayOfMs * dayCount;
		final long startMs = yesterdayMs - durationMs;
		
		return between ( startMs, yesterdayMs );
	}

	public BuxferTransactionFilter lastCalendarMonth ()
	{
		// get the last day of last month...
		final Calendar cal = Calendar.getInstance ();
		cal.add ( Calendar.MONTH, -1 );
		cal.set ( Calendar.DAY_OF_MONTH, cal.getActualMaximum ( Calendar.DAY_OF_MONTH ) );
		final Date lastDay = cal.getTime ();
		cal.set ( Calendar.DAY_OF_MONTH, 1 );
		final Date firstDay = cal.getTime ();
		
		return between ( firstDay, lastDay );
	}

	public Map<String,String> toQueryArgs ()
	{
		final HashMap<String,String> map = new HashMap<>();

		if ( fAcctId != null ) 
		{
			map.put ( "accountId", fAcctId );
		}
		else if ( fAcctName != null )
		{
			map.put ( "accountName", fAcctName );
		}

		if ( fTagId != null ) 
		{
			map.put ( "tagId", fTagId );
		}
		else if ( fTagName != null )
		{
			map.put ( "tagName", fTagName );
		}

		if ( fStartDate != null )
		{
			map.put ( "startDate", fStartDate );
			map.put ( "endDate", fEndDate == null ? fStartDate : fEndDate );
		}
		
		return map;
	}
	
	private String fAcctId = null;
	private String fAcctName = null;
	private String fTagId = null;
	private String fTagName = null;
	private String fStartDate = null;
	private String fEndDate = null;

	private static String epochMsToString ( long dateEpochMs )
	{
		return dateToString ( new Date ( dateEpochMs ) );
	}

	private static String dateToString ( Date date )
	{
		return new SimpleDateFormat ( "yyyy-MM-dd" ).format ( date );
	}
}
