package com.rathravane.buxferClient;

import java.util.List;

public interface BuxferTransaction
{
	String getId ();
	
	String getAcctId ();

	String getDescription ();
	
	/**
	 * Get the transaction date as an epoch date in seconds.
	 * @return an epoch value in seconds.
	 */
	long getDate ();
	
	BuxferAmount getAmount ();

	List<String> getTags ();
}
