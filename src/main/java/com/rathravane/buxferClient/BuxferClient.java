package com.rathravane.buxferClient;

import java.io.IOException;
import java.util.Collection;

public interface BuxferClient
{
	/**
	 * Log this session out
	 */
	void logout () throws IOException;

	/**
	 * Get the accounts for this client
	 * @return a collection of accounts
	 * @throws IOException
	 */
	Collection<BuxferAccount> getAccounts () throws IOException;

	/**
	 * Get the transactions matching the given filter
	 * @param filter
	 * @param handler
	 * @throws IOException
	 */
	void getTransactions ( BuxferTransactionFilter filter, BuxferCallback<BuxferTransaction> handler ) throws IOException;
}
