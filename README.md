# buxferJavaClient
Some code for pulling data from Buxfer.

## Features
This code currently just pulls account and transaction data. Feel free to pull-request additional functionality!

## Example

	public void sumAndAvg () throws IOException
	{
		final BuxferClient client = new BuxferClientFactory ()
			.withLoginUserId ( System.getenv ( "BUXFER_USERID" ) )
			.withLoginPassword ( System.getenv ( "BUXFER_PASSWORD" ) )
			.build ()
		;

		final int dayCount = 30;
		final Summer summer = new Summer ();

		client.getTransactions ( 
			new BuxferTransactionFilter ()
				.onAccountWithId ( System.getenv ( "BUXFER_ACCTID" ) )
				.betweenYesterdayAndPriorDays ( dayCount )
			,
			summer
		);

		final double avg = summer.getTotal () / dayCount;

		log.info ( "Sum for last " + dayCount + " days ending yesterday: "  + summer.getTotal () );
		log.info ( "Avg for last " + dayCount + " days ending yesterday: "  + avg );
	}

	private static final Logger log = LoggerFactory.getLogger ( SumAndAvgOverDaysTest.class );

	private static class Summer implements BuxferCallback<BuxferTransaction>
	{
		private double fTotal = 0.0;

		@Override
		public void handle ( BuxferTransaction t )
		{
			fTotal += t.getAmount ().toDouble ();
		}

		public double getTotal () { return fTotal; }
		
	}
